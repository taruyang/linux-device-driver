/*
** Copyright (C) 2007 Google, Inc.
**
** This software is licensed under the terms of the GNU General Public
** License version 2, as published by the Free Software Foundation, and
** may be copied, distributed, and modified under those terms.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
*/
/**
 *  @file    goldfish_sensor.c
 *  @author  Jongho Yang (17078054)
 *  @date    31/3/2017
 *  @version 1.0
 *
 *  @brief	 goldfish sensor device driver
 *
 *  @section DESCRIPTION
 *
 *  This is a platform device driver program to access registers
 *  for three sensors, such as accelerometer, compass and gyroscopes
 *  and to control interrupt occurrence from them.
 *  The requested operation can be accomplished by linux system calls
 *  onto the device file at /dev/sensor.
 *  Also, the user space could acquire the detail log information
 *  when debugging is required by turning on a Module Parameters
 *  for debugging.
 *
 * - How to access the goldfish sensor device driver
 * 		Access the below device driver file by file I/O system calls
 * 			/dev/sensor
 *
 * - System calls
 * 		int open(const char *pathname, int flags);
 * 			To open the sensor device file, use "/dev/sensor" for pathname.
 * 			The  argument  flags  must  include  one of the following access modes:
 *      	O_RDONLY, O_WRONLY, or O_RDWR.
 *
 * 		ssize_t read(int fd, void *buf, size_t count);
 * 			This call returns current all register values for the three sensors.
 * 			The buf size has to be bigger than READ_BUFFER_SIZE which is sizeof(struct sensors).
 * 			The user space can easily parse the return data set if the blow structure is used.
 * 			The read function for goldfish sensor is a blocking I/O.
 * 			It will sleep until there are real changes among three sensors.
 * 			The sleeping will be waked up when a sensor value is changed.
 * 			The user space can utilize the read API to do required operation when there are real changes.
 *
 * 			struct sensor_values {
 * 				uint32_t x;
 * 				uint32_t y;
 * 				uint32_t z;
 * 			};
 *
 * 			struct sensors {
 * 				uint32_t		which;	/// indicate that which sensor caused an interrupt
 * 				struct sensor_values		accel;
 * 				struct sensor_values		compass;
 * 				struct sensor_values		gyro;
 * 			};
 *
 *			The register values are read when an interrupt occurs.
 *
 *		ssize_t write(int fd, const void *buf, size_t count);
 * 			Users can enable or disable interrupts by writing a combination value of below bits
 * 			through the buf.
 * 				The bit 0 of the value is for the accelerometer control.
 *				The bit 1 of the value is for the compass control.
 *				The bit 2 of the value is for the gyroscopes control.
 *			If a value is 1, it enables interrupt for specified sensor.
  *			For example, 7, which is 111 in binary system, enables all interrupts.
 *			and 4, 011, enables the compass and accelerometer interrupts only.
 *			The gyroscopes interrupt will be disabled in this case.
 *
 *			Warning!!! The value should be a char value, ascii code value of a numeric number.
 *			So if you want to set 7, it should be '7', not numeric 7.
 *			You could test in the shell like below.
 *				echo 7 > /dev/sensor
 *
 *			Warning!!! The user space has to write a value into the first one byte in the buf
 *			This driver checks first byte only.
 *
 *		int ioctl(int fd, unsigned long command, unsigned long arg);
 * 			The user space can enable or disable an interrupt for a sensor without
 * 			knowing somewhat difficult bit operation by this API.
 * 			Currently, 3 commands are prepared for this purpose.
 * 				SENSOR_IOCTL_ACCEL for the accelerometer.
 * 				SENSOR_IOCTL_COMPASS for the compass.
 * 				SENSOR_IOCTL_GYRO for gyroscope.
 * 			Each interrupt will be enabled when the value of the arg is 1.
 * 			and be disabled when it is 0.
 * 			All ioctl commands have been tested by another sample program running on the emulator.
 *
 *		int close(int fd);
 *			Close file
 *
 * - Module Parameters
 * 	 debug_mode : a flag to enable debug mode. Detail log information will be displayed.
 * 					 	1 : enable debug mode.
 * 					 	0 : disable debug mode (default)
 * 				  e.g ) echo 1 > /sys/module/goldfish_sensor/parameters/debug_mode
 */
#include <linux/module.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/platform_device.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/types.h>
#include <linux/pci.h>
#include <linux/interrupt.h>
#include <linux/kthread.h>		// for threads
#include <linux/time.h>  		// for using jiffies
#include <asm/types.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#ifdef	CONFIG_X86
#include <asm/mtrr.h>
#endif
#ifdef CONFIG_ARM
#include <mach/hardware.h>
#endif

MODULE_AUTHOR("Joseph Yang 17078054");
MODULE_DESCRIPTION("Android Goldfish Sensor Driver");
MODULE_LICENSE("GPL");
MODULE_VERSION("1.0");

/**
 *	Header part
 *		This part would be moved to a header file, like "goldfish_sensor.h"
 *		to be referred from the user space for easy access and understanding
 *		to sensor driver file at /dev/sensor
 */

/**
 * @brief	bit mask to enable or disable interrupt from specified sensor
 */
enum {
	SENSOR_INT_ACCEL	= 1U << 0,
	SENSOR_INT_COMPASS	= 1U << 1,
	SENSOR_INT_GYRO     = 1U << 2,

	SENSOR_INT_MASK     = SENSOR_INT_ACCEL | SENSOR_INT_COMPASS | SENSOR_INT_GYRO
};

/**
 * @brief 	defines for ioctl api for the user space
 */
#define SENSOR_IOCTL_MAGIC 		's'
#define SENSOR_IOCTL_ACCEL      _IOW(SENSOR_IOCTL_MAGIC, 1, unsigned int)
#define SENSOR_IOCTL_COMPASS    _IOW(SENSOR_IOCTL_MAGIC, 2, unsigned int)
#define SENSOR_IOCTL_GYRO       _IOW(SENSOR_IOCTL_MAGIC, 3, unsigned int)

/**
 * @brief	Type declaration for the user space
 */
struct sensor_values {
	uint32_t x;
	uint32_t y;
	uint32_t z;
};

struct sensors {
	uint32_t					which;	/// indicate that which sensor caused an interrupt
	struct sensor_values		accel;
	struct sensor_values		compass;
	struct sensor_values		gyro;
};

#define READ_BUFFER_SIZE        (sizeof(struct sensors))

/**
 *	The end of header part
 */

#define GOLDFISH_SENSOR_READ(data, addr)		(readl(data->reg_base + addr))
#define GOLDFISH_SENSOR_WRITE(data, addr, x)	(writel(x, data->reg_base + addr))
#define DBG_PRINT if(debug_mode) printk

/**
 * @brief	Each registers' offset values from base address
 */
enum{
	/** Set this to enable IRQ */
	SENSOR_INT_ENABLE	= 0x00,
	/** A read from this register shows which sensor caused an interrupt
	 * and also acknowledges the interrupt.
	 * A write to this register enables interrupts for each of the sensors
	 */

	/** These registers return the X, Y and Z values for the accelerometer. */
	SENSOR_ACCEL_X = 0x04,
	SENSOR_ACCEL_Y = 0x08,
	SENSOR_ACCEL_Z = 0x0c,

	/** These registers return the X, Y and Z values for the accelerometer. */
	SENSOR_COMPASS_X  = 0x10,
	SENSOR_COMPASS_Y  = 0x14,
	SENSOR_COMPASS_Z  = 0x18,

	/** These registers return the X, Y and Z values for the gyroscope */
	SENSOR_GYRO_X  = 0x1c,
	SENSOR_GYRO_Y  = 0x20,
	SENSOR_GYRO_Z  = 0x24
};

/**
 * @brief	Module Parameters
 */
static int debug_mode = 1;
module_param(debug_mode, int, 0644);
MODULE_PARM_DESC(debug_mode, "debug mode enable flag");

/**
 * @brief	goldfish sensor driver data structure
 */
struct goldfish_sensor {
	/** Resources from board */
	char __iomem *				reg_base;
	int 						irq;

	/** For concurrency protection */
	spinlock_t 					lock;
	wait_queue_head_t 			wait;

	/** Actural values for each sensor */
	struct sensors 				sensors;
	uint32_t					enables;

	char 						is_updated;
};

/**
 * @brief	Global variables
 */
static struct goldfish_sensor* sensor_data;
static atomic_t open_count = ATOMIC_INIT(0);

/**
 * @brief	print all register values in the goldfish_sensor struct
 * 			This function only will be enabled in the debug_mode
 * @param  	data struct goldfish_sensor pointer.
 */
static void goldfish_sensor_print_data(struct goldfish_sensor* data)
{
	if(!debug_mode)
		return;

	printk("[Sensor Driver] %s \n", __FUNCTION__);

	printk("[Sensor Driver] ===== Print all registers stored in data ===== \n");

	printk("[Sensor Driver] \t SENSOR_INT_ENABLE : 0x%x \n", data->sensors.which);

	printk("[Sensor Driver] \t SENSOR_ACCEL_X : 0x%x \n", data->sensors.accel.x);
	printk("[Sensor Driver] \t SENSOR_ACCEL_Y : 0x%x \n", data->sensors.accel.y);
	printk("[Sensor Driver] \t SENSOR_ACCEL_Z : 0x%x \n", data->sensors.accel.z);

	printk("[Sensor Driver] \t SENSOR_COMPASS_X : 0x%x \n", data->sensors.compass.x);
	printk("[Sensor Driver] \t SENSOR_COMPASS_Y : 0x%x \n", data->sensors.compass.y);
	printk("[Sensor Driver] \t SENSOR_COMPASS_Z : 0x%x \n", data->sensors.compass.z);

	printk("[Sensor Driver] \t SENSOR_GYRO_X : 0x%x \n", data->sensors.gyro.x);
	printk("[Sensor Driver] \t SENSOR_GYRO_Y : 0x%x \n", data->sensors.gyro.y);
	printk("[Sensor Driver] \t SENSOR_GYRO_Z : 0x%x \n", data->sensors.gyro.z);

	printk("[Sensor Driver] ============================================== \n");
}

/**
 * @brief	Read x, y, z values for the accelerometer
 * @param  	data struct goldfish_sensor pointer.
 */
static void goldfish_sensor_read_accel(struct goldfish_sensor* data)
{
	data->sensors.accel.x = GOLDFISH_SENSOR_READ(data, SENSOR_ACCEL_X);
	data->sensors.accel.y = GOLDFISH_SENSOR_READ(data, SENSOR_ACCEL_Y);
	data->sensors.accel.z = GOLDFISH_SENSOR_READ(data, SENSOR_ACCEL_Z);
}

/**
 * @brief	Read x, y, z values for the magnetometer
 * @param  	data struct goldfish_sensor pointer.
 */
static void goldfish_sensor_read_compass(struct goldfish_sensor* data)
{
	data->sensors.compass.x = GOLDFISH_SENSOR_READ(data, SENSOR_COMPASS_X);
	data->sensors.compass.y = GOLDFISH_SENSOR_READ(data, SENSOR_COMPASS_Y);
	data->sensors.compass.z = GOLDFISH_SENSOR_READ(data, SENSOR_COMPASS_Z);
}

/**
 * @brief	Read x, y, z values for the gyroscope
 * @param  	data struct goldfish_sensor pointer.
 */
static void goldfish_sensor_read_gyro(struct goldfish_sensor* data)
{
	data->sensors.gyro.x = GOLDFISH_SENSOR_READ(data, SENSOR_GYRO_X);
	data->sensors.gyro.y = GOLDFISH_SENSOR_READ(data, SENSOR_GYRO_Y);
	data->sensors.gyro.z = GOLDFISH_SENSOR_READ(data, SENSOR_GYRO_Z);
}

/**
 * @brief	Read x, y, z values of sensors which caused interrupts
 * @param  	data struct goldfish_sensor pointer.
 */
static void goldfish_sensor_read_register(struct goldfish_sensor* data)
{
	data->sensors.which = GOLDFISH_SENSOR_READ(data, SENSOR_INT_ENABLE);

	if(data->sensors.which & SENSOR_INT_ACCEL)
		goldfish_sensor_read_accel(data);
	if(data->sensors.which & SENSOR_INT_COMPASS)
		goldfish_sensor_read_compass(data);
	if(data->sensors.which & SENSOR_INT_GYRO)
		goldfish_sensor_read_gyro(data);
}

/**
 * @brief	Called when user-space uses the read() system call on the device
 * 			This read function is a blocking I/O. So it will sleep until there are real changes
 * 			among three sensors. The sleeping will be waked up by the interrupt handler in this file.
 * @param  	fp		a pointer to the same file structure passed in the open() operation
 * @param  	buf 	user space buffer
 * @param  	count 	size to read
 * @retval 	pos	   	current position in the file
 * @retval 	ssize_t	the number of byte read
 */
static ssize_t goldfish_sensor_read(struct file *fp, char __user *buf,
							size_t count, loff_t *pos)
{
	struct goldfish_sensor* data = fp->private_data;

	DBG_PRINT("[Sensor Driver] %s, count %d, current pos %d \n", __FUNCTION__, count, (int)*pos);

	wait_event_interruptible(data->wait, data->is_updated);

	DBG_PRINT("[Sensor Driver] %s, is_updated %d \n", __FUNCTION__, (int)data->is_updated);

	data->is_updated = 0;

	count = READ_BUFFER_SIZE;

	goldfish_sensor_print_data(data);

	/* copy all current values to user space */
	if (copy_to_user(buf, &(data->sensors), count))
	{
		printk("[Sensor Driver] %s, copy_from_user failed!\n", __FUNCTION__);
		return -EFAULT;
	}

	*pos += count;

	DBG_PRINT("[Sensor Driver] %s, success to read %d bytes \n", __FUNCTION__, (int)count);

	return count;
}

/**
 * @brief	Called when user-space uses the write() system call on the device
 *
 * 			Users can enable or disable interrupts by writing register value.
 * 				Bit 0 controls the accelerometer.
 *				Bit 1 controls the compass.
 *				Bit 2 controls the gyroscopes
 *			So, by setting 7 which is 111 in binary system, all interrupts will be enabled.
 *			and by setting 4, 011, the compass and accelerometer interrupts will be activated.
 *			Warning!!! The value should be a char value. So if you want to set 7, it should be '7'.
 *
 *			Warning!!! Users have to write a value into the first one byte in the buf
 *			This driver checks first byte
 *
 * @param  	fp		a pointer to the same file structure passed in the open() operation
 * @param  	buf 	user space buffer
 * @param  	count 	size to write
 * @retval 	pos	   	current position in the file
 * @retval 	ssize_t	the number of byte write
 */
static ssize_t goldfish_sensor_write(struct file *fp, const char __user *buf,
							 size_t count, loff_t *pos)
{
	unsigned long irq_flags;
	struct goldfish_sensor* data = fp->private_data;
	char user_data;

	DBG_PRINT("[Sensor Driver] %s \n", __FUNCTION__);

	if(count < 1)
	{
		printk("[Sensor Driver] %s, Failed. Buffer size is smaller than requested size !\n", __FUNCTION__);
		return -EFAULT;
	}

	/// Just read one byte
	get_user(user_data, buf);
	data->enables = user_data;

	spin_lock_irqsave(&data->lock, irq_flags);
	GOLDFISH_SENSOR_WRITE(sensor_data, SENSOR_INT_ENABLE, data->enables);
	spin_unlock_irqrestore(&data->lock, irq_flags);

	DBG_PRINT("[Sensor Driver] write %u to modify INT_ENABLE register !\n", data->enables);

	return count;
}

/**
 * @brief	Called when user-space opens the sensor device file at "/dev/sensor".
 * @param  	ip		a structure that uniquely represent a file in the system
 * @param  	fp	 	a structure created every time a file is opened. Several file
 * 					structures can point to the same inode structure.
 * @retval 	int		result of operation
 */
static int goldfish_sensor_open(struct inode *ip, struct file *fp)
{
	DBG_PRINT("[Sensor Driver] %s \n", __FUNCTION__);

	if (!sensor_data)
		return -ENODEV;

	if (atomic_inc_return(&open_count) > 0)
	{
		fp->private_data = sensor_data;
		return 0;
	}
	else
	{
		atomic_dec(&open_count);
		return -EBUSY;
	}
}

/**
 * @brief	Called when user-space closes the file
 * @param  	ip		a structure that uniquely represent a file in the system
 * @param  	fp	 	a structure created every time a file is opened.
 * @retval 	int		result of operation
 */
static int goldfish_sensor_release(struct inode *ip, struct file* fp)
{
	DBG_PRINT("[Sensor Driver] %s \n", __FUNCTION__);

	atomic_dec(&open_count);

	return 0;
}

/**
 * @brief	Called when user-space call the ioctl() system call
 *
 * 			The user space can enable or disable an interrupt for a sensor without
 * 			knowing detail enable bit information for it.
 * 			Current, there are 3 commands for this purpose.
 * 				SENSOR_IOCTL_ACCEL for the accelerometer.
 * 				SENSOR_IOCTL_COMPASS for the compass.
 * 				SENSOR_IOCTL_GYRO for gyroscope.
 * 			Each interrupt will be enabled when the value of the arg is 1.
 * 			and be disabled when it is 0
 *
 * @param  	fp		a pointer to the same file structure passed in the open() operation
 * @param  	cmd 	a number identifying the operation to perform
 * @param  	arg 	optional argument
 * @retval 	int		result of operation
 */
static long goldfish_sensor_ioctl(struct file* fp, unsigned int cmd, unsigned long arg)
{
	unsigned long irq_flags;
	struct goldfish_sensor* data = fp->private_data;
	uint32_t reg, mask;
	DBG_PRINT("[Sensor Driver] %s, cmd %d, val %ld \n", __FUNCTION__, cmd, arg);

	switch (cmd) {
	case SENSOR_IOCTL_ACCEL:
	case SENSOR_IOCTL_COMPASS:
	case SENSOR_IOCTL_GYRO:
		spin_lock_irqsave(&data->lock, irq_flags);
		reg = data->enables;
		mask = (cmd == SENSOR_IOCTL_ACCEL) ? SENSOR_INT_ACCEL :
				(cmd == SENSOR_IOCTL_COMPASS) ? SENSOR_INT_COMPASS :
						SENSOR_INT_GYRO;

		data->enables = (arg > 0) ? reg | mask : reg & ~mask ;
		GOLDFISH_SENSOR_WRITE(sensor_data, SENSOR_INT_ENABLE, data->enables);
		spin_unlock_irqrestore(&data->lock, irq_flags);

		DBG_PRINT("[Sensor Driver] write %u to modify INT_ENABLE register !\n", data->enables);
		break;
	default:
			printk(KERN_INFO "unknown ioctl: %d\n", cmd);
			return -EINVAL;
	}

	return 0;
}

/**
 * @brief	file operations for /dev/sensor
 */
static struct file_operations goldfish_sensor_fops = {
	.owner = THIS_MODULE,
	.read = goldfish_sensor_read,
	.write = goldfish_sensor_write,
	.open = goldfish_sensor_open,
	.release = goldfish_sensor_release,
	.unlocked_ioctl = goldfish_sensor_ioctl,
};

/**
 * @brief	interrupt handler
 *			Registers related with current interrupt are read.
 */
static irqreturn_t
goldfish_sensor_interrupt(int irq, void *dev_id)
{
	unsigned long irq_flags;
	struct goldfish_sensor	*data = dev_id;

	spin_lock_irqsave(&data->lock, irq_flags);

	goldfish_sensor_read_register(data);

	data->is_updated = 1;

	wake_up_interruptible(&data->wait);

	DBG_PRINT("[Sensor Driver] %s, wake_up \n", __FUNCTION__);

	spin_unlock_irqrestore(&data->lock, irq_flags);

	DBG_PRINT("[Sensor Driver] %s, completed \n", __FUNCTION__);

	return IRQ_HANDLED;
}

/**
 * @brief	device information
 */
static struct miscdevice goldfish_sensor_device = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "sensor",
	.fops = &goldfish_sensor_fops,
	.mode = S_IRUGO | S_IWUSR
};

/**
 * @brief  probe sensor device
 *          This method is responsible for initializing the hardware,
 * 			registering the device to the proper framework
 *
 * @param  	pdev platform device information for sensor device
 * @retval 	0   Succeed
 * @retval 	val < 0  Error code
 */
static int goldfish_sensor_probe(struct platform_device *pdev)
{
	int ret;
	struct resource *r;
	struct goldfish_sensor *data;

	printk("[Sensor Driver] %s \n", __FUNCTION__);

	data = kzalloc(sizeof(*data), GFP_KERNEL);
	if(data == NULL) {
		ret = -ENOMEM;
		goto err_data_alloc_failed;
	}

	spin_lock_init(&data->lock);
	init_waitqueue_head(&data->wait);
	platform_set_drvdata(pdev, data);

	r = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if(r == NULL) {
		printk("[Sensor Driver] %s, platform_get_resource failed\n", __FUNCTION__);
		ret = -ENODEV;
		goto err_no_io_base;
	}

	data->reg_base = (char __iomem *)IO_ADDRESS(r->start - IO_START);
	data->irq = platform_get_irq(pdev, 0);
	if(data->irq < 0) {
		printk("[Sensor Driver] %s, platform_get_irq failed\n", __FUNCTION__);
		ret = -ENODEV;
		goto err_no_irq;
	}

	ret = request_irq(data->irq, goldfish_sensor_interrupt, IRQF_SHARED, pdev->name, data);
	if(ret)
		goto err_request_irq_failed;

	if((ret = misc_register(&goldfish_sensor_device)))
	{
		printk("[Sensor Driver] %s, misc_register returned %d in goldfish_sensor_init\n", __FUNCTION__, ret);
		goto err_misc_register_failed;
	}

	sensor_data = data;
	sensor_data->enables |= SENSOR_INT_MASK;
	GOLDFISH_SENSOR_WRITE(sensor_data, SENSOR_INT_ENABLE, SENSOR_INT_MASK);

	return 0;

err_misc_register_failed:
err_request_irq_failed:
err_no_irq:
err_no_io_base:
	kfree(data);
err_data_alloc_failed:
	return ret;
}

/**
 * @brief  release all resources which are allocated the sensor device
 */
static int goldfish_sensor_remove(struct platform_device *pdev)
{
	struct goldfish_sensor *data = platform_get_drvdata(pdev);

	printk("[Sensor Driver] %s \n", __FUNCTION__);

	GOLDFISH_SENSOR_WRITE(sensor_data, SENSOR_INT_ENABLE, 0);

	misc_deregister(&goldfish_sensor_device);
	free_irq(data->irq, data);
	kfree(data);
	sensor_data = NULL;

	return 0;
}

static struct platform_driver goldfish_sensor_driver = {
	.probe		= goldfish_sensor_probe,
	.remove		= goldfish_sensor_remove,
	.driver = {
		.name = "goldfish_sensor"	/**< This name should be same with that of sensor device. Joseph Yang */
	}
};

/**
 * @brief  module init function
 */
static int __init goldfish_sensor_init(void)
{
	int ret;

	printk("[Sensor Driver] %s \n", __FUNCTION__);

	ret = platform_driver_register(&goldfish_sensor_driver);
	if (ret < 0)
	{
		printk("[Sensor Driver] platform_driver_register returned %d\n", ret);
		return ret;
	}

	printk("[Sensor Driver] goldfish platform_sensor driver_register is succedded \n");

	return ret;
}

/**
 * @brief  module exit function
 */
static void __exit goldfish_sensor_exit(void)
{
	printk("[Sensor Driver] %s \n", __FUNCTION__);

	platform_driver_unregister(&goldfish_sensor_driver);
}

module_init(goldfish_sensor_init);
module_exit(goldfish_sensor_exit);