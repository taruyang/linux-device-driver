# README #

- file    goldfish_sensor.c
- author  Jongho Yang (Joseph.Yang.MIITP@gmail.com)
- date    31/3/2017
- version 1.0
- brief	 goldfish sensor device driver

# DESCRIPTION #
  This is a linux platform device driver program to access registers
  for three sensors, such as accelerometer, compass and gyroscopes
  and to control interrupt occurrence from them.
  The requested operation can be accomplished by linux system calls
  onto the device file at /dev/sensor.
  Also, the user space could acquire the detail log information
  when debugging is required by turning on a Module Parameters
  for debugging.

# How to access the goldfish sensor device driver #
  Access the below device driver file by file I/O system calls /dev/sensor

# System calls #
	int open(const char *pathname, int flags);
		To open the sensor device file, use "/dev/sensor" for pathname.
		The  argument  flags  must  include  one of the following access modes:
     	O_RDONLY, O_WRONLY, or O_RDWR.
 		ssize_t read(int fd, void *buf, size_t count);
			This call returns current all register values for the three sensors.
			The buf size has to be bigger than READ_BUFFER_SIZE which is sizeof(struct sensors).
			The user space can easily parse the return data set if the blow structure is used.
			The read function for goldfish sensor is a blocking I/O.
			It will sleep until there are real changes among three sensors.
			The sleeping will be waked up when a sensor value is changed.
			The user space can utilize the read API to do required operation when there are real changes.

			struct sensor_values {
				uint32_t x;
 				uint32_t y;
				uint32_t z;
			};

 			struct sensors {
				uint32_t		which;	/// indicate that which sensor caused an interrupt
 				struct sensor_values		accel;
				struct sensor_values		compass;
 				struct sensor_values		gyro;
 			};

			The register values are read when an interrupt occurs.

		ssize_t write(int fd, const void *buf, size_t count);
 			Users can enable or disable interrupts by writing a combination value of below bits
 			through the buf.
 				The bit 0 of the value is for the accelerometer control.
				The bit 1 of the value is for the compass control.
				The bit 2 of the value is for the gyroscopes control.
			If a value is 1, it enables interrupt for specified sensor.
			For example, 7, which is 111 in binary system, enables all interrupts.
			and 4, 011, enables the compass and accelerometer interrupts only.
			The gyroscopes interrupt will be disabled in this case.

			**Warning!!!** The value should be a char value, ascii code value of a numeric number.
			So if you want to set 7, it should be '7', not numeric 7.
			You could test in the shell like below.
				echo 7 > /dev/sensor

			Warning!!! The user space has to write a value into the first one byte in the buf
			This driver checks first byte only.

		int ioctl(int fd, unsigned long command, unsigned long arg);
 			The user space can enable or disable an interrupt for a sensor without
 			knowing somewhat difficult bit operation by this API.
 			Currently, 3 commands are prepared for this purpose.
 				SENSOR_IOCTL_ACCEL for the accelerometer.
 				SENSOR_IOCTL_COMPASS for the compass.
 				SENSOR_IOCTL_GYRO for gyroscope.
 			Each interrupt will be enabled when the value of the arg is 1.
 			and be disabled when it is 0.
 			All ioctl commands have been tested by another sample program running on the emulator.

		int close(int fd);
			Close file

# Module Parameters #
	 debug_mode : a flag to enable debug mode. Detail log information will be displayed.
		 	1 : enable debug mode.
		 	0 : disable debug mode (default)
 				  e.g ) echo 1 > /sys/module/goldfish_sensor/parameters/debug_mode