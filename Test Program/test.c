#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define BUFF_SIZE 40

/// definition for IOCTL
#define IOCTL_INT_ACCEL 		10
#define IOCTL_INT_COMPASS 		11
#define IOCTL_INT_GYRO	 		12

int main(void)
{
	int fd, ret;
	unsigned int* buffer;

	fd = open("/dev/sensor", O_RDWR);
	buffer = (unsigned int*)malloc(BUFF_SIZE);

	ret = ioctl(fd, IOCTL_INT_ACCEL, 1);
	ret = ioctl(fd, IOCTL_INT_COMPASS, 1);
	ret = ioctl(fd, IOCTL_INT_GYRO, 1);
	ret = ioctl(fd, IOCTL_INT_ACCEL, 0);
	read(fd, (void*)buffer, BUFF_SIZE);
	ret = ioctl(fd, IOCTL_INT_COMPASS, 0);
	read(fd, (void*)buffer, BUFF_SIZE);
	ret = ioctl(fd, IOCTL_INT_GYRO, 0);
	read(fd, (void*)buffer, BUFF_SIZE);

	return 0;
}

